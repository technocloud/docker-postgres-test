FROM postgres:13-alpine

COPY ./docker_database_init.sh /docker-entrypoint-initdb.d/docker_database_init.sh
RUN chmod u+x /docker-entrypoint-initdb.d/docker_database_init.sh
