#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "postgres" <<-EOF
  CREATE EXTENSION IF NOT EXISTS dblink;
  DO \$$
    BEGIN
      IF NOT EXISTS (SELECT FROM pg_roles WHERE rolname = '$DB_USER') THEN
        CREATE USER $DB_USER WITH CREATEDB;
        ALTER USER $DB_USER PASSWORD '$DB_PASSWORD';
      END IF;

      IF NOT EXISTS (SELECT FROM pg_database WHERE datname = '$DB_NAME') THEN
        PERFORM dblink_exec('', 'CREATE DATABASE $DB_NAME');
        GRANT ALL PRIVILEGES ON DATABASE $DB_NAME TO $DB_USER;
      END IF;
    END
    \$$;
EOF

export PGDATABASE=$DB_NAME
